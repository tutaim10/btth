﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using Windows.ApplicationModel.Contacts;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FormLogin
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ObservableCollection<Contact> Contacts;

        public MainPage()
        {
            this.InitializeComponent();
            Contacts = new ObservableCollection<Contact>();
        }
        private void NewContactButton_Click(object sender, RoutedEventArgs e)
        {
            Contacts.Add(new Contact { UserName = UserName.Text , Password= Password.Text, Name= Name.Text, Address= Address.Text, Email= Email.Text, About= About.Text, ZipCode= ZipCode.Text });
            UserName.Text = "";
            Password.Text = "";
            Name.Text = "";
            Address.Text = "";
            Email.Text = "";
            About.Text = "";
            ZipCode.Text = "";
        }

    }
}
